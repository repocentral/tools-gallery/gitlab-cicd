# gitlab-cicd

This is a project which shows a CI/CD pipeline built using Gitlab CI. The original gitlab pipeline can be found [here]()

### Steps to prepare this

- Pull a python flask app to local, pushed to gitlab. 
- Make a `.gitlab-ci.yml` file and start adding content to it. This file can have code that can run tests, build images, deploy in many resources etc.
-  Each job will run in a new dedicated container. All the managed runners which Gitlab provides runs on Docker container. Gitlab's managed runners use Ruby image to run the container. 
- But we here Ruby isn't required, Python is required. For this overwrite needs to be done over the Ruby image. Here, the image is being tagged.
```yml
run_test:
  image: python:3.9-slim-buster
  script:
    - make tests
```
- The project used here needs _make, python_ and _pip_. So these needs to be available in the runner also. The jobs of this yml can be executed on a docker container. The above lacks the prerequisites, those needs to be mentioned. _before_script_ is the attribute that will do the job here. _make_ will install all the requird softwares in the container.
```yml
run_test:
  image: python:3.9-slim-buster
  before_script:
    - apt-get update && apt-get install make
  script:
    - make tests
```

